import React, { useEffect, useState } from 'react';
import * as ReactDOM from "react-dom";
import { BrowserRouter, Link } from "react-router-dom";

import { v4 as uuidv4 } from 'uuid';

import IconButton from '@mui/material/IconButton';
import Badge from '@mui/material/Badge';
import ShoppingCartOutlinedIcon from '@mui/icons-material/ShoppingCartOutlined';

import { addItem, removeItem } from "@acid/sspa-utility";

export default function CartButton() {
  const [items, setItems] = useState(0);

  useEffect(() => {
    
    const processItem = () => {
    setItems(items + 1);
    const itemId = uuidv4();
    const itemName = 'Product' + items + '/' + itemId;
    addItem(itemName, 'MockUser');
  };
    
    const deleteLastItem = () => {
    setItems(Math.max(items - 1, 0));
    if(items > 0){
     removeItem(); 
    }
  }
    
    const handleEvent = (event) => {
      event.detail ? processItem() : deleteLastItem();
    };

    document.addEventListener('cartAction', handleEvent);

    return () => {
      document.removeEventListener('cartAction', handleEvent);
    };
  }, [items]);

  return (
    <BrowserRouter>
      <IconButton aria-label="cart">
        <Link to="/checkout" className="link">
          <Badge badgeContent={items} color="success">
            <ShoppingCartOutlinedIcon color="action"/>
          </Badge>
        </Link>
      </IconButton>
    </BrowserRouter>
  );
}